// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45

#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess {

public:
	// This default constructor initializes a board with the standard
	// piece positions, and sets the state to white's turn
	Chess();

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& get_board() const { return board; }

	// Returns true if it is white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white() const { return is_white_turn; }

	// Attemps to make a move. If successful, the move is made and
	// the turn is switched white <-> black
	bool make_move(std::pair<char, char> start, std::pair<char, char> end);

	// Returns true if the designated player is in check
	bool in_check(bool white) const;



  // Returns true if the designated player is in mate
	bool in_mate(bool white) const;

	// Returns true if the designated player is in mate
	bool in_stalemate(bool white) const;

	//Helper function to check for valid moves
	bool validMove(std::pair<char, char> start, std::pair<char, char> end) const;
  //helpers to detect valid moves 
  bool vertical_movement(std::pair<char, char> start, std::pair<char, char> end) const;
  bool horizontal_movement(std::pair<char, char> start, std::pair<char, char> end) const;
  bool diagonal_movement(std::pair<char, char> start, std::pair<char, char> end) const;

  ~Chess() {} //Chess destructor
  Chess(const Chess& chess) : board(chess.board), is_white_turn(chess.is_white_turn) {} //Chess copy constructor

  void operator=(const Chess& chess); //Overloaded assignment operator for Chess

  void set_board(Board temp) {
    board = temp;
  }

  void set_white_turn(bool white) {
    is_white_turn = white;
  }

private:
	// The board
	Board board;

	// Is it white's turn?
	bool is_white_turn;

};

// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess);

// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess);


#endif // CHESS_H
