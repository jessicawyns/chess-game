// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45

#include "Pawn.h"
#include <cmath>

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{

  //check movements

  if(!is_white()) {
    if(start.second == '7'){
      bool invalidSpacesBlack = start.second - end.second < 1 || start.second - end.second > 2;
      
	if (invalidSpacesBlack) {
	  return false;
	}
    }
        else{
	  if( start.second - end.second != 1){
	    return false;
	  }
        }
  }
  else{
      // if white
    if(start.second == '2') {
	bool invalidSpacesWhite = end.second - start.second < 1 || end.second - start.second > 2; 
	if (invalidSpacesWhite) {
	  return false;
	}
    }
    else{
	if(end.second - start.second != 1){
	  return false; 
	}
      }
    }

  if ( start.first != end.first){
    return false; 
  }
  
  return true;
}
bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const{

  if (!is_white()){
    if(start.second - end.second != 1){
          return false;
    }
  }

  else if(is_white()){

    if(end.second - start.second != 1){
          return false;
    }
  }
  int dist1 = std::abs(start.first - end.first);
  int dist2 = std::abs(start.second - end.second);
  if(dist1 != dist2) {
    return false;
  }

  //if (start.first != end.first){
  //  return false;
  //}
  return true;
}
