// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45


#include <cmath>
#include "Queen.h"

bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  int firstDiff = std::abs((int)end.first - (int)start.first);
  int secondDiff = std::abs((int)end.second - (int)start.second);

  bool notStraight = end.first != start.first && end.second != start.second;
  bool notDiagonal = secondDiff != firstDiff;

  if (notStraight && notDiagonal) {
    return false;
  }

  return true;
 
}

