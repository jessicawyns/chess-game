// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45


#include <cmath>
#include "King.h"

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const{
  
  double firstDiff = std::pow((double)start.first - (double)end.first, 2);
  double secondDiff = std::pow((double)start.second - (double)end.second, 2);

  double dist = std::sqrt(firstDiff + secondDiff);

  if (dist > std::sqrt(2)) {
    return false;
  }

  return true;

}


