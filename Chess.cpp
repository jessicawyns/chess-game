// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45



#include "Chess.h"
#include <cmath>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
	// Add the pawns
	for (int i = 0; i < 8; i++) {
    
		board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
		board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
	}

	// Add the rooks
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
	board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
	board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
	board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
	board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
	board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
	board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
	board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
	board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}

bool Chess::validMove(std::pair<char, char> start, std::pair<char, char> end) const {
  //Check if start and end are same position

  if (start == end) {
      return false;
  }
  
  //Check that the starting position has piece
  if (!board(start)) {
      return false;
  }

  //Check you are moving your own color piece
  bool samePieceColor = board(start)->is_white() == is_white_turn;
  if (!samePieceColor) {
    return false;
  } 

  //Check start and end are within the board
  bool invalidStart = start.first < 'A' || start.first > 'H' || start.second < '1' || start.second > '8';
  bool invalidEnd = end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8';
  if (invalidStart || invalidEnd) {
      return false;
  }
  
  //Check if piece is blocking movement of a piece
  bool validMoveShape = board(start)->legal_move_shape(start, end);
  bool validCaptureShape = board(start)->legal_capture_shape(start, end);
  if (validMoveShape && !validCaptureShape && board(end)) {
    return false;
  }
  
  //Check for faulty capture when no end piece and bad move
  if (!validMoveShape && validCaptureShape && !board(end)) {
    return false;
  }
  
  //Check if bad move and bad capture
  if (!validMoveShape && !validCaptureShape) {
    return false;
  }

  //Check if capturing your own piece
  if (board(end) && (board(start)->is_white() == board(end)->is_white())) {
    return false;
  } 

  //check for if piece is inthe way of the move 
  if(board(start) -> legal_move_shape(start, end) || board(start)-> legal_capture_shape(start, end)) {

    //vertical movements
    if(end.first == start.first){
      return vertical_movement(start,end);
    }
        
    else if (start.second == end.second){
      return horizontal_movement(start,end);
    }
    else if(std::abs((int)(end.second - start.second)) == std::abs((int)(end.first - start.first))){
      return diagonal_movement(start,end);
    }

  }    
  return true; 

    //make temporary copy of the Board
 // check if start points to null

}




bool Chess::vertical_movement(std::pair<char, char> start, std::pair<char, char> end) const {
  std::pair<char,char> holder = start;
  int difference = end.second - start.second; 
  if(difference > 0 ){
    for(int i = 0; i < difference - 1; i++){
      holder.second++;
      if(board(holder)){
	return false; // piece is in the way 
      }
    } 

  }else{ //downward
    for(int j = 0; j < start.second - end.second - 1; j++){
      holder.second--;
      if(board(holder)){
	return false; //piece is in the way 
      }

    }

  }
  return true; 
}


bool Chess::horizontal_movement(std::pair<char, char> start, std::pair<char, char> end) const {
 std::pair<char,char> holder = start;
  if(end.first> start.first){// right
    for(int i = 0; i < end.first - start.first - 1; i++){
   holder.first++;
      if(board(holder)){
        return false; // piece is in the way
      }

    }
  }
  else{ // left

   for(int j = 0; j < start.first - end.first - 1; j++){
      holder.first--;
      if(board(holder)){

        return false; //piece is in the way                                                                          
      }
    }
  }

  return true; 
}
bool Chess::diagonal_movement(std::pair<char, char> start, std::pair<char, char> end) const {
 std::pair<char,char> holder = start;
  // upward diagonal
  
 if(end.second > start.second){
   //right
   if(end.first> start.first){
   for(int i = 0; i < end.first - start.first - 1; i++){
     holder.first++;
     holder.second++;
     if(board(holder)){
        return false; // piece is in the way 
     }
   }
   }
   else{//left
     for(int j = 0; j < start.first - end.first - 1; j++){
      holder.first--;
      holder.second++;
      if(board(holder)){
        return false; //piece is in the way
      }	      
     }
   }
 }
 
 else{ // down ward diagonal 
  if(end.first > start.first){
   for(int k = 0; k < end.first - start.first - 1; k++){
   holder.first++;
   holder.second--;
   if(board(holder)){
        return false; // piece is in the way	
   }
   }
  }else{//left
     for(int p = 0; p < start.first - end.first - 1; p++){
      holder.first--;
      holder.second--;
      if(board(holder)){
        return false; //piece is in the way
	
      }					
     }

  }
 } 
  return true; 
}

bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
  
  Board copyBoard(board);
  //Check if move is valid
  if (!validMove(start, end)) {
    return false;
  }



  //Update pieces if move made
  board.remove_piece(end);
  board.add_piece(end, board(start)->to_ascii());
  board.remove_piece(start);

  //Check for Pawn to Queen promotion
  bool blackPawnPromote = board(end)->to_ascii() == 'p' && end.second == '1';
  bool whitePawnPromote = board(end)->to_ascii() == 'P' && end.second == '8';
  if (blackPawnPromote) {
    board.remove_piece(end);
    board.add_piece(end, 'q');
  }
  if (whitePawnPromote) {
    board.remove_piece(end);
    board.add_piece(end, 'Q');
  }

  //Check to see if still in check if in check, revert board
  if (in_check(is_white_turn)) {
    board = copyBoard;
    return false;
  }

  //Swap sides
  
  is_white_turn = !is_white_turn;
  return true;
}

//Assignment operator
void Chess::operator=(const Chess& chess) {
  board = chess.board;
  is_white_turn = chess.is_white_turn;
}

bool Chess::in_check(bool white) const {
  
  Chess copyChess(*this);
  
  //Determine the coordinates of the king on the board
  std::pair<char, char> kingLocation;
  char row;
  char col;
  std::pair<char,char> temp; 
  for (col = 'A'; col < 'I'; col++) {
    for (row = '1'; row < '9'; row++) {
      temp = std::pair<char, char>(col, row);
      if (copyChess.board(temp) &&
	 copyChess.board(temp)->is_white() == white) {
	char k1 = copyChess.board(temp)->to_ascii();
	char k2 = copyChess.board(temp)->to_ascii();
        if (k1 == 'K' || k2  == 'k') {
          kingLocation.first = col;
          kingLocation.second = row;
        }
      }
    }
  } 

  //Swap turns and check for pieces putting in check
  copyChess.is_white_turn = !is_white_turn;
  for (col = 'A'; col < 'I'; col++) {
    for (row = '1'; row < '9'; row++) {
      if (copyChess.validMove(std::pair<char, char>(col, row), kingLocation)) {
        return true;
      }
    }
  } 

  return false;

}



bool Chess::in_mate(bool white) const {

  Chess copyChess(*this);

  //Must be in check to have checkmate
  if (!in_check(white)) {
    return false;
  }

  //Get position
  std::pair<char , char> pos;
  char col;
  char row;
  std::pair<char,char> p;
  for (row = '1'; row < '9' ;row ++) {
    for (col = 'A'; col <= 'H'; col++) {
      p = std::pair<char,char>(col,row);
      if (board(p) && board(p)->is_white() == white) {
        pos.first = col;
        pos.second = row;

        //For all positions, check if move can be made to another position
        for (char r = '8'; r >= '1'; r--) {
          for (char c = 'A'; c <= 'H'; c++) {
            if (copyChess.make_move(pos, std::pair<char, char>(c, r))) {               
              return false;
            }
          }
        }
      }
    }
  }
  //No moves available, meaning in checkmate
  return true;

}


bool Chess::in_stalemate(bool white) const {
if(!in_check(white)){ 
    
    Chess tempChess(*this);
    
    std::pair<char, char> position;
    // check that there are no possible moves without putting oneself in check
    for (char r = '8'; r >= '1'; r--) {
      for (char c = 'A'; c <= 'H'; c++) {
	if (board(std::pair<char, char>(c, r)) && board(std::pair<char, char>(c, r))->is_white() == white) {
	  position.first = c;
	  position.second = r;
	  for (char row = '8'; row >= '1'; row--) {
	    for (char col = 'A'; col <= 'H'; col++) {
	      if(tempChess.make_move(position, std::pair<char, char>(col, row))){
		return false;
	      }
	    }
	  }
	}
      }
    }
    return true;
  }
  else{
    return false;
  }


}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {

  // Write the board out and then either the character 'w' or the character 'b',
	// depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


std::istream& operator>> (std::istream& is, Chess& chess) {
char ch; char r; char c;
  Board toPrint;

    for(r = '8'; r >= '1'; r--){
      for(c = 'A'; c <= 'H'; c++){
        is>>ch; 
        toPrint.add_piece(std::pair<char,char>(c,r),ch);
      }
    }
    is >> ch;
    chess.set_white_turn(ch == 'w');
    chess.set_board(toPrint);
        return is;

  /////////////////////////
	// [REPLACE THIS STUB] //
	/////////////////////////

}
