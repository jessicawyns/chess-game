#Simon Cha - Scha7
#Jessica Nguyen - Jnguye45
CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


chess: main.o Board.o Chess.o CreatePiece.o Pawn.o King.o Rook.o Knight.o Bishop.o Queen.o Mystery.h
	$(CC) main.o Board.o Pawn.o Rook.o King.o Knight.o Bishop.o Queen.o Chess.o CreatePiece.o Mystery.h -o chess -lm

main.o: main.cpp Chess.h Piece.h Board.h Bishop.h Pawn.h Rook.h Knight.h Queen.h King.h Mystery.h 
	$(CC) $(CFLAGS) -c main.cpp

Board.o: Board.cpp Board.h CreatePiece.h Piece.h Pawn.h Terminal.h Rook.h Knight.h Bishop.h Queen.h King.h Mystery.h 
	$(CC) $(CFLAGS) -c Board.cpp

Chess.o: Chess.cpp Piece.h Piece.h Board.h Pawn.h Rook.h Bishop.h  Mystery.h Queen.h King.h Knight.h Chess.h
	$(CC) $(CFLAGS) -c Chess.cpp

CreatePiece.o: CreatePiece.cpp CreatePiece.h Bishop.h Piece.h Rook.h Knight.h  Queen.h King.h Mystery.h Pawn.h
	$(CC) $(CFLAGS) -c CreatePiece.cpp

King.o: King.cpp King.h Piece.h
	$(CC) $(CFLAGS) -c King.cpp

Bishop.o: Bishop.cpp Bishop.h Piece.h
	$(CC) $(CFLAGS) -c Bishop.cpp

Knight.o: Knight.cpp Knight.h Piece.h
	$(CC) $(CFLAGS) -c Knight.cpp

Queen.o: Queen.cpp Queen.h Piece.h
	$(CC) $(CFLAGS) -c Queen.cpp

Pawn.o: Pawn.cpp Pawn.h Piece.h
	$(CC) $(CFLAGS) -c Pawn.cpp

Rook.o: Rook.cpp Rook.h Piece.h
	$(CC) $(CFLAGS) -c Rook.cpp

clean:
	rm -f *.o chess
