// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45


#include <cmath>
#include "Rook.h"

bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {

  bool notStraight = end.first != start.first && end.second != start.second;

  if (notStraight) {
    return false;
  }

  return true;

}

