// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45


#ifndef PAWN_H
#define PAWN_H

#include "Piece.h"

class Pawn : public Piece {

public:
  //method to detect legal movement
  bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;
  // method to detect legal capture
  bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const;

	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'P' : 'p';
	}

	~Pawn() {}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Pawn(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // PAWN_H
