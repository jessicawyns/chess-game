// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45

#include <iostream>
#include <utility>
#include <map>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"

using std::cout;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board(){}

//Copy constructor for the board
Board::Board(const Board& board){
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      if(board(std::pair<char, char>(c, r))) {
	add_piece(std::pair<char, char>(c, r), board(std::pair<char, char>(c, r))->to_ascii());
      }
    }
  }
}


//Assignment operator= overload
void Board::operator=(const Board& board) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      remove_piece(std::pair<char, char>(c, r));
      if(board(std::pair<char, char>(c, r))) {
	add_piece(std::pair<char, char>(c, r), board(std::pair<char, char>(c, r))->to_ascii());
      }
    }
  }
}


//Desctructor for the board
Board::~Board() {
	for (std::map<std::pair<char, char>, Piece*>::iterator it = occ.begin();
		it != occ.end(); it++) {
	  delete it->second;
	}
}


const Piece* Board::operator()(std::pair<char, char> position) const {
	std::map<std::pair<char, char>, Piece*>::const_iterator it = occ.find(position);
	if (it != occ.end()) {
	  return it->second;
	}
	return NULL;
}

bool Board::remove_piece(std::pair<char, char> position) {
   // check valid position
  //Check if invalid position or already held
  bool invalidFirst = position.first < 'A' || position.first > 'H';
  bool invalidSecond = position.second < '1' || position.second > '8';
  if (invalidFirst || invalidSecond) {
    return false;
  }

  //Check if exists, remove if found
  bool exists = occ.find(position) == occ.end();
  if (!exists) {
    delete (occ.find(position))->second;
    occ.erase(position);
    }
  return true; 
}



bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
	//Check if invalid position or already held
	bool invalidFirst = position.first < 'A' || position.first > 'H';
	bool invalidSecond = position.second < '1' || position.second > '8';
	if (invalidFirst || invalidSecond || occ[position]) {
	  return false;
	}

	if(occ[position] != NULL) {
	  return false; // piece is already there 
	}else{
	
	occ[position] = create_piece(piece_designator);

	}
	//Check if the piece designator was invalid
	if (occ[position] == NULL) {
	  return false;
	}

	return true;
}

bool Board::has_valid_kings() const {
 int king1 = 0; // white piece counter
  int king2 = 0; // black piece counter
  char row;
  char col;

  for (row = '1'; row < '9'; row++) {
    for( col = 'A'; col <= 'H'; col++){
      const Piece* p = (*this)(std::pair<char,char>(col,row));
      if(p != NULL){
      if(p -> to_ascii() == 'k'){
	king2++;
      }else if( p -> to_ascii() == 'K'){
	king1++; 
      }
      }
    }

  }

  if( king1 != 1 || king2 != 1){
    return false; // each player does not have one king
  }

  return true;
}


void Board::display() const {
  int i = 0; 
  for(char r = '8'; r > '0'; r--) {
    cout<< std::endl<< r << "  ";
   for(char c = 'A'; c <= 'H'; c++) {
     if(i % 2 == 0 ){
       Terminal::color_bg(Terminal::RED);
     } else{
       Terminal::color_bg(Terminal::GREEN);
     }

     if((*this)(std::pair<char,char>(c,r))){
       const Piece* p = (*this)(std::pair<char,char>(c,r)); 
       if(p-> is_white()){	   
	   Terminal::color_fg(true, Terminal::WHITE);
           cout<< p-> to_ascii();
       }
       else{
           Terminal::color_fg(false, Terminal::BLACK);
	   cout<< p -> to_ascii();
       }
     } else{
	 cout<<" ";	 
     }
     Terminal::set_default();
     i++;
   }
   i++;
  }
  cout << std::endl << "   ABCDEFGH" << std::endl; 
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<<(std::ostream& os, const Board& board) {
	for(char r = '8'; r >= '1'; r--) {
		for(char c = 'A'; c <= 'H'; c++) {
			const Piece* piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
			  os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}

