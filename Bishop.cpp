// Simon Cha - Scha7
// Jessica Nguyen - Jnguye45

#include <cmath>
#include "Bishop.h"

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  //ensures diagonal movements 
  int firstDiff = std::abs((int)end.first - (int)start.first); 
  int secondDiff = std::abs((int)end.second - (int)start.second);

  if (secondDiff != firstDiff) {
    return false;
  }

  return true;

}
